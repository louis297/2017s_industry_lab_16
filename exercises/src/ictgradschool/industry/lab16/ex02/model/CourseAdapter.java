package ictgradschool.industry.lab16.ex02.model;


import javax.swing.*;
import javax.swing.table.AbstractTableModel;

public class CourseAdapter extends AbstractTableModel implements CourseListener{

    private Course course;
    private String[] columnNames = {"Student ID", "Surname", "Forename", "Exam", "Test", "Assignment", "Overall"};

    public CourseAdapter(Course course) {
        this.course = course;
    }


    public String getColumnName(int col) {
        return columnNames[col];
    }

    public Class getColumnClass(int c) {
        return getValueAt(0, c).getClass();
    }

    @Override
	public void courseHasChanged(Course course) {
        try {
            Thread.sleep(200);
        } catch (InterruptedException e) {

        }
        System.out.println(course.size());
//        for (int i = 0; i < course.size(); i++) {
//            System.out.println(course.getResult(i));
//
//        }
        this.course = course;
        fireTableDataChanged();
	}

    @Override
    public int getRowCount() {
        return course.size();
    }

    @Override
    public int getColumnCount() {
        return (course.size() == 0) ? 0 : 7;
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        switch(columnIndex){
            case 0:
                return course.getResult(rowIndex)._studentID;
            case 1:
                return course.getResult(rowIndex)._studentSurname;
            case 2:
                return course.getResult(rowIndex)._studentForename;
            case 3:
                return course.getResult(rowIndex).getAssessmentElement(StudentResult.AssessmentElement.Exam);
            case 4:
                return course.getResult(rowIndex).getAssessmentElement(StudentResult.AssessmentElement.Test);
            case 5:
                return course.getResult(rowIndex).getAssessmentElement(StudentResult.AssessmentElement.Assignment);
            case 6:
                return course.getResult(rowIndex).getAssessmentElement(StudentResult.AssessmentElement.Overall);

        }
        return null;
    }

    /**********************************************************************
	 * YOUR CODE HERE
	 */
}